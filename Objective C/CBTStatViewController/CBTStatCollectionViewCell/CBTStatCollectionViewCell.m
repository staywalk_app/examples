#import "CBTStatCollectionViewCell.h"

@implementation CBTStatCollectionViewCell

-(void)setupWithUserEventModelArray:(NSArray *)items {
    
    int total = 0;
    
    NSDate *date;
    
    [self.statHeaderItem configureChart];
    
    for (CBTUserEventStatModel *item in items) {
        
        if (!date) {
            date = item.mDate;
        }
        
        int amount = [item.mAmount intValue];
        
        [self.statHeaderItem addSlice:[NSNumber numberWithInt:amount] withColor:[UIColor whsdColorForListIconWithHash:[item.mPan longLongValue]]];
        
        total += amount;
    }
    
    [self.statHeaderItem updateChart];
    [self.statHeaderItem setupWithDate:date amount:[NSNumber numberWithInt:total]];
}

@end
