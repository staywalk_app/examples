#import "CBTStatTableCell.h"

@implementation CBTStatTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setupWithEventStat:(CBTUserEventStatModel *)eventStat {
    
    self.imgIcon.layer.cornerRadius = self.imgIcon._height/2;
    [self.imgIcon setClipsToBounds:YES];
    [self.imgIcon setBackgroundColor:[UIColor whsdColorForListIconWithHash:[eventStat.mPan longLongValue]]];
    
    [self.lblTitle setAttributedString:eventStat.mAlias];
    [self.lblPan setAttributedString:eventStat.mPan];
    [self.lblAmount setAttributedString:[NSString stringWithFormat:@"-%@ ₽", eventStat.mAmount]];
}

@end
