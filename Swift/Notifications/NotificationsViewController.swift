import UIKit
import Stripe

class NotificationsViewController: ManagedObjectListViewController<ListItem,NotificationViewCell> {
    
    // MARK: Override
    
    override class var storyboardName: String { return "Notifications" }
    override var isModalViewController: Bool { return true }
    
    override var sortDescriptorInfos: [(String, Bool)] { return [("mIndex", true)] }
    override var rowAnimation: UITableViewRowAnimation { return isUpdatingList ? .none : .automatic }
    override var hasRefresh: Bool { return true }
    override var hasPagination: Bool { return true }
    
    private var listType: ListItem.ListType = .notifications
    private var isUpdatingList: Bool = false
    private var hasMore: Bool = true
    
    override func navBarRightButtonTapped() {
        popOrDismissSelf()
    }
    
    // MARK: Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 155
        tableView.rowHeight = UITableViewAutomaticDimension
        
        updateObjectList(forced: true) {
            ApiCore.sharedInstance.readAllNotification()
        }
    }
    
    override func predicate(with search: String?) -> NSPredicate {
        return listType.predicate
    }
    
    override func beginRefreshing(completion: @escaping () -> Void) {
        updateObjectList(forced: true, completion: completion)
    }
    
    override func beginLoadingNextPage(completion: @escaping () -> Void) {
        
        guard !isUpdatingList, hasMore else {
            completion()
            return
        }
        
        updateObjectList(forced: false, completion: completion)
    }
    
    override func setupCell(_ cell: NotificationViewCell, with object: ListItem, in tableView: UITableView, at indexPath: IndexPath) {
        guard let notification = object.mUserNotification else { return }
        cell.setup(with: notification)
        cell.delegate = self
    }
    
    override func didSelectRow(with object: ListItem, in tableView: UITableView, at indexPath: IndexPath) {
        
        super.didSelectRow(with: object, in: tableView, at: indexPath)
        
        guard let notification = object.mUserNotification else { return }
        
        switch notification.mRequestType {
        case .friend:
            guard let user = notification.mUser else { return }
            ContactViewController.pushInNavVC(navigationController, animated: true) { (vc: ContactViewController) in
                vc.contact = user
            }
        case .team:
            guard let team = notification.mTeam else { return }
            TeamViewController.pushInNavVC(navigationController, animated: true) { (vc: TeamViewController) in
                vc.team = team
            }
        case .tournament:
            guard let tournament = notification.mTournament else { return }
            TournamentViewController.pushInNavVC(navigationController, animated: true) { (vc: TournamentViewController) in
                vc.tournament = tournament
            }
        }
    }    
    
    // MARK: Private
    
    private func updateObjectList(forced: Bool, completion: (() -> Void)? = nil) {
        isUpdatingList = true
        
        ApiCore.sharedInstance.updateObjectList(listType, forced: forced, search: nil) { [weak self] (hasMore, error) in
            guard let strongSelf = self else { return }
            strongSelf.hasMore = hasMore
            strongSelf.isUpdatingList = false
            completion?()
        }
    }
    
    private func createPaymentSource(for tournament: Tournament, with invoice: Invoice) {
        
        if invoice.mAmount?.mValue == 0 {
            proceedInvoice(invoice, tournament: tournament, with: nil)
        } else {
            PaymentViewController.pushInNavVC(navigationController, animated: true) { (vc: PaymentViewController) in
                vc.tournament = tournament
                vc.invoice = invoice
            }
        }
    }
    
    private func proceedInvoice(_ invoice: Invoice, tournament: Tournament, with source: STPSource? ) {
        
        ApiCore.sharedInstance.processInvoice(tournament, invoice: invoice, credentialsId: source?.stripeID) { [weak self] (invoice, error) in
            if invoice == nil {
                self?.showError(error)
                return
            }
        }
    }
}

// MARK:- NotificationViewCellDelegate

extension NotificationsViewController: NotificationViewCellDelegate {
    
    func notificationViewCellAccept(_ cell: NotificationViewCell, notification: UserNotification) {
        
        notification.setIsUsed(true)
        
        switch notification.mRequestType {
        case .friend:
            if let contact = notification.mUser {
                ApiCore.sharedInstance.changeFriendStatusWithContact(contact, upgrade: true, completion: { [weak self] (contact, error) in
                    self?.notificationViewProcessRequest(cell, notification: notification, error: error)
                })
            }
        case .team:
            if let team = notification.mTeam {
                ApiCore.sharedInstance.acceptMembershipWithTeam(team, completion: { [weak self] (team, error) in
                    self?.notificationViewProcessRequest(cell, notification: notification, error: error)
                })
            }
        case .tournament:
            if let tournament = notification.mTournament, let team = notification.mTeam {
                ApiCore.sharedInstance.acceptInviteToTournament(tournament, team: team, completion: { [weak self] (invoice, error) in
                    
                    guard let invoice = invoice else {
                        self?.showError(error)
                        return
                    }
                    
                    self?.createPaymentSource(for: tournament, with: invoice)
                })
            }
        }        
    }
    
    func notificationViewCellDecline(_ cell: NotificationViewCell, notification: UserNotification) {
        
        notification.setIsUsed(true)
        
        switch notification.mRequestType {
        case .friend:
            if let contact = notification.mUser {
                ApiCore.sharedInstance.changeFriendStatusWithContact(contact, upgrade: false, completion: { [weak self] (contact, error) in
                    self?.notificationViewProcessRequest(cell, notification: notification, error: error)
                })
            }
        case .team:
            if let team = notification.mTeam {
                ApiCore.sharedInstance.leaveTeam(team, completion: { [weak self] (team, error) in
                    self?.notificationViewProcessRequest(cell, notification: notification, error: error)
                })
            }
        case .tournament:
            if let tournament = notification.mTournament, let team = notification.mTeam {
                ApiCore.sharedInstance.rejectInviteToTournament(tournament, team: team, completion: { [weak self] (error) in
                    self?.notificationViewProcessRequest(cell, notification: notification, error: error)
                })
            }
        }
    }
    
    func notificationViewProcessRequest(_ cell: NotificationViewCell, notification: UserNotification, error: Error?) {
        
        if error != nil {
            notification.setIsUsed(false)
        }
    }
}
