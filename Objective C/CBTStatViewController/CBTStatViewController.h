#import "CBTViewController.h"
#import "CBTTransponderModel.h"
#import "CBTStatTableCell.h"
#import "CBTStatHeaderItem.h"
#import "CBTStatCollectionViewCell.h"

@interface CBTStatViewController : CBTViewController <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *statCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *statTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lctHeaderBackgroundTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lctHeaderBackgroundHeight;

@property (weak, nonatomic) IBOutlet UIButton *collectionViewArrowRight;
@property (weak, nonatomic) IBOutlet UIButton *collectionViewArrowLeft;

@end
