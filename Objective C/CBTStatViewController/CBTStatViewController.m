#import "CBTStatViewController.h"
#import "CBTModalPushAnimator.h"
#import "CBTModalPopAnimator.h"
#import "CBTApiCore+Methods.h"
#import "CBTUserEventStatModel.h"
#import "UIScrollView+SVPullToRefresh.h"

@interface CBTStatViewController ()

@end

#define kMinYear 2017

@implementation CBTStatViewController {
    
    CBTNavigationBar* mNavigationBar;
    UIButton* mBtnBack;
    
    NSArray* statArray;
    int statCollectionViewPrevIndex;
}

-(void) awakeFromNib {
    [super awakeFromNib];
    [self setCustomPushAnimator:[[CBTModalPushAnimator alloc] init]];
    [self setCustomPopAnimator:[[CBTModalPopAnimator alloc] init]];
    [self setMDisableInteractivePopGesture:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setMActivityIndicatorBackgoroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    
    [self configureNavigationBar];
    [self configureUI];
    
    [self prepareData];
}

#pragma mark - Data

- (void)prepareData {
    
    [self updateData];
    
    [self collectionViewScrollToIndex:statArray.count - 1];
    
    if (statArray.count == 0) {
        
        __weak typeof(self) weakSelf = self;
        
        [self requestDataWithDate:[NSDate date] completion:^(BOOL success, NSError *error) {
            
            [weakSelf updateData];
            [weakSelf requestMore];
        }];
    }
}

- (void)updateData {
    
    statArray = [self groupArray:[CBTUserEventStatModel getAllStatOrderByDate] byKey:@"mDate"];
    
    if (statArray.count == 0) {
        
        [self collectionViewUpdateArrowVisibility];
        
        return;
    }
    
    [self.statCollectionView reloadData];
}

- (void)requestMore {
    
    if ([self collectionViewCurrentIndex] == 0) {
        [self requestDataWithDate:[self getPreviusMonthFromCurrentEvent] scrollTo:1];
    } else {
        [self.statTable reloadData];
    }
}

- (void)requestDataWithDate:(NSDate *)date scrollTo:(long)index {
    
    __weak typeof(self) weakSelf = self;
    
    long prevCount = statArray.count;
    
    [self requestDataWithDate:date completion:^(BOOL success, NSError *error) {
        
        [weakSelf.statTable.pullToRefreshView stopAnimating];
        
        [weakSelf updateData];
        
        if (error) {
            [weakSelf displayError:error];
            return;
        }
        
        if (prevCount < statArray.count) {
            [weakSelf collectionViewScrollToIndex:index];
        }
    }];
}

- (void)requestDataWithDate:(NSDate *)date completion:(void (^)(BOOL success, NSError *error))completion {
    
    [self showActivityViewAnimated:YES];
    
    __weak typeof(self) weakSelf = self;
    
    [[CBTApiCore sharedInstance] getUserEventStatWithDate:[self getFirstDayWithDate:date] completion:^(BOOL success, NSError *error) {
        
        [weakSelf hideActivityViewAnimated:YES];
        
        completion(success,error);
    }];
}

- (NSDate *)getDateFromCurrentEvent {
    
    NSArray *userEventStatArray = [statArray objectAtIndex:[self collectionViewCurrentIndex]];
    CBTUserEventStatModel *userEventStatModel = [userEventStatArray firstObject];
    
    return userEventStatModel.mDate;
}

- (NSDate *)getPreviusMonthFromCurrentEvent {
    
    NSDate *date = [self getDateFromCurrentEvent];
    return [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:date options:0];
}

#pragma mark - UI

- (void)configureUI {
    
    [self.statTable layoutIfNeeded];
    
    [self.collectionViewArrowRight setAlpha:0];
    
    __weak typeof(self) weakSelf = self;
    
    [self.statTable addPullToRefreshWithActionHandler:^{
        [weakSelf requestDataWithDate:[self getDateFromCurrentEvent] scrollTo:[self collectionViewCurrentIndex]];
    }];
}

#pragma mark - NavigationBar

- (void)configureNavigationBar {
    
    mNavigationBar = [[CBTNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view._width, kNavigationBarHeight)];
    [mNavigationBar setBackgroundColor:[UIColor whsdDownyColor]];
    [mNavigationBar setTitle: NSLocalizedStringWithKey(@"statVC_Title", @"-CBTStatVC")];
    
    mBtnBack = [UIButton whsdNavigationButtonWithImage:[UIImage imageNamed:@"icnClose"]];
    [mBtnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [mNavigationBar setLeftButtons:@[mBtnBack]];
    [self.view addSubview: mNavigationBar];
}

#pragma mark - IBActions

- (void)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)collectionViewArrowLeftTap:(id)sender {
    [self collectionViewScrollToIndex:0];
}

- (IBAction)collectionViewArrowRightTap:(id)sender {
    [self collectionViewScrollToIndex:statArray.count - 1];
}

#pragma mark - statCollectionView

- (int)collectionViewCurrentIndex {
    
    int index = round(self.statCollectionView.contentOffset.x / self.statCollectionView._width);
    
    if (index < 0) {
        index = 0;
    }
    
    if (index > statArray.count - 1) {
        index = (int)statArray.count - 1;
    }
    
    return index;
}

- (bool)collectionViewDidScroll {
    
    int index = [self collectionViewCurrentIndex];
    bool result = statCollectionViewPrevIndex != index;
    
    statCollectionViewPrevIndex = index;
    
    return result;
}

- (void)collectionViewScrollToIndex:(long)index {
    
    if (index < 0 || index > statArray.count - 1) {
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection: 0];
    [self.statCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionRight animated:NO];
    
    [self.statTable reloadData];
    
    [self collectionViewUpdateArrowVisibility];
}

- (void)collectionViewUpdateArrowVisibility {
    
    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        
        if ([self collectionViewCurrentIndex] > 0) {
            
            [self.collectionViewArrowLeft setAlpha:1];
        } else {
            [self.collectionViewArrowLeft setAlpha:0];
        }
        
        if (statArray.count > 0 && [self collectionViewCurrentIndex] < statArray.count - 1) {
            
            [self.collectionViewArrowRight setAlpha:1];
        } else {
            [self.collectionViewArrowRight setAlpha:0];
        }
    } completion:^(BOOL finished){ }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return statArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CBTStatCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CBTStatCollectionViewCell" forIndexPath:indexPath];
    
    [cell setupWithUserEventModelArray:statArray[indexPath.row]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return collectionView._size;
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return statArray.count > 0 ? [[statArray objectAtIndex:[self collectionViewCurrentIndex]] count] : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *userEventStatArray = [statArray objectAtIndex:[self collectionViewCurrentIndex]];
    
    CBTStatTableCell *cell = [tableView dequeueReusableCellWithIdentifier: @"CBTStatTableCell"];
    [cell setupWithEventStat:[userEventStatArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ([scrollView isEqual:self.statCollectionView]) {
        
        if ([self collectionViewDidScroll]) {
            [self requestMore];
        } else {
            [self collectionViewUpdateArrowVisibility];
            [self.statTable reloadData];
        }
    }
}

#pragma mark - Helpers

- (NSArray*)sortDescriptorForTransponders {
    NSSortDescriptor *statusSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mStatusRef" ascending:YES];
    NSSortDescriptor *aliasSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mAlias" ascending:YES];
    return @[statusSortDescriptor, aliasSortDescriptor];
}

- (NSArray *)groupArray:(NSArray *)items byKey:(NSString *)key  {
    
    if (items.count == 0) {
        return nil;
    }
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSMutableArray *group  = [[NSMutableArray alloc] init];
    
    NSObject *prevValue;
    NSObject *value;
    
    for (NSObject *item in items) {
        
        value = [item valueForKey:key];
        
        if (prevValue != nil && ![value isEqual:prevValue]) {
            [result addObject:[group copy]];
            [group removeAllObjects];
        }
        
        [group addObject:item];
        
        prevValue = [item valueForKey:key];
    }
    
    [result addObject:[group copy]];
    
    return result;
}

- (NSDate *)getFirstDayWithDate:(NSDate *)date {
    
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components: NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    [dateComponents setDay:1];
    
    return [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
}

- (NSDate *)getPreviusMonth:(NSDate *)date {
    return [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:date options:0];
}

@end
