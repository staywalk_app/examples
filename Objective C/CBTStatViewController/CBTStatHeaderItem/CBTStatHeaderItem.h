#import <UIKit/UIKit.h>
#import "CBTNibView.h"
#import "XYPieChart.h"

@interface CBTStatHeaderItem : CBTNibView <XYPieChartDelegate, XYPieChartDataSource>

@property (weak, nonatomic) IBOutlet UIView *centerView;
@property (weak, nonatomic) IBOutlet XYPieChart *statChart;

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property(nonatomic, strong) NSMutableArray *slices;
@property(nonatomic, strong) NSMutableArray *sliceColors;
@property(nonatomic, strong) NSDate *date;

- (void) configureChart;
- (void) setupWithDate:(NSDate *)date amount:(NSNumber *)amount;
- (void) addSlice:(NSNumber *)value withColor:(UIColor *)color;
- (void) clearChart;
- (void) updateChart;

@end
