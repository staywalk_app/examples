#import "CBTStatHeaderItem.h"

@implementation CBTStatHeaderItem {
}

- (void) setupWithDate:(NSDate *)date amount:(NSNumber *)amount {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"LLLL, yyyy";
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = kCFNumberFormatterDecimalStyle;
    
    [self.lblDate setAttributedString:[[dateFormatter stringFromDate:date] uppercaseString]];
    [self.lblAmount setAttributedString: [NSString stringWithFormat:@"-%@ ₽", [numberFormatter stringFromNumber:amount]]];
   
    self.date = date;
}

- (void) configureChart {
    [self.statChart setDelegate:self];
    [self.statChart setDataSource:self];
    [self.statChart setStartPieAngle:M_PI_2];
    [self.statChart setPieRadius:self.statChart._width/2];
    [self.statChart setAnimationSpeed:0.4];
    [self.statChart setShowLabel:NO];
    [self.statChart setShowPercentage:NO];
    [self.statChart setPieBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1]];
    [self.statChart setPieCenter:CGPointMake(self.statChart._width/2, self.statChart._width/2)];
    [self.statChart setUserInteractionEnabled:NO];
    
    self.slices = [[NSMutableArray alloc] init];
    self.sliceColors = [[NSMutableArray alloc] init];
    
    self.centerView.layer.cornerRadius = self.centerView._width/2;
    [self.centerView setClipsToBounds:YES];
}

- (void) addSlice:(NSNumber *)value withColor:(UIColor *)color; {
    [self.slices addObject:value];
    [self.sliceColors addObject:color];
}


- (void) clearChart {
    [self.slices removeAllObjects];
    [self.sliceColors removeAllObjects];
}

- (void) updateChart {
    [self.statChart reloadData];
}

#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart {
    return self.slices.count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index {
    return [[self.slices objectAtIndex:index] intValue];
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index{
    return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
}

- (void) headerScrollViewDidScroll:(CGFloat)offset {
    
}

#pragma mark - XYPieChart Delegate

- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index {
    
}

- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index {
    
}

- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index {
    
}

- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index {
    
}

@end
