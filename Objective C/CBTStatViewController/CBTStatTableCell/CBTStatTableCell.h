#import <UIKit/UIKit.h>
#import "CBTUserEventStatModel.h"

@interface CBTStatTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPan;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lctAmountWidth;

- (void)setupWithEventStat:(CBTUserEventStatModel *)eventStat;

@end
