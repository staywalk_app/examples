#import <UIKit/UIKit.h>
#import "CBTStatHeaderItem.h"
#import "CBTUserEventStatModel.h"

@interface CBTStatCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet CBTStatHeaderItem *statHeaderItem;

-(void)setupWithUserEventModelArray:(NSArray *)items;

@end
