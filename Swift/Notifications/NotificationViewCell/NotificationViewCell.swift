import UIKit

protocol NotificationViewCellDelegate {
    
    func notificationViewCellAccept(_ cell: NotificationViewCell, notification: UserNotification)
    func notificationViewCellDecline(_ cell: NotificationViewCell, notification: UserNotification)
}

class NotificationViewCell: ObjectViewCell<UserNotification> {
    
    var delegate: NotificationViewCellDelegate?
    
    private var initial: Bool = false
    
    @IBOutlet weak private var lblName: CBLabel!
    @IBOutlet weak private var lblRequestType: CBLabel!
    @IBOutlet weak private var lblSquadName: CBLabel!
    @IBOutlet weak private var btnDecline: CBButton!
    @IBOutlet weak private var btnAccept: CBButton!
    @IBOutlet weak private var imgPreview: PhotoView!
    @IBOutlet weak private var buttonsView: UIView!
    
    // MARK: - Override
    
    override func setup(with notification: UserNotification) {
        super.setup(with: notification)
        
        if !initial {
            btnAccept.addTarget(self, action: #selector(acceptTap), for: .touchUpInside)
            btnDecline.addTarget(self, action: #selector(declineTap), for: .touchUpInside)
            initial = true
        }
        
        setStatusText(type: notification.mType)
        
        switch notification.mType {
            
        case .friendRequest,
             .friendAccepted,
             .friendRejected:
            
            setObject(with: notification.mUser)
            
        case .teamRequest: setObject(with: notification.mTeam)
            
        case .teamAccepted,
             .teamRejected:
            
            setObject(with: notification.mUser, addition: notification.mTeam?.mName)
            
        case .tournamentRequest:
            
            setObject(with: notification.mTournament, addition: notification.mTeam?.mName)
            
        case .tournamentTeamAccepted,
             .tournamentTeamRejected:
            
            setObject(with: notification.mTournament)
            
        case .tournamentUserAccepted,
             .tournamentUserRejected:
            
            setObject(with: notification.mUser, addition: notification.mTournament?.mName)
        }
        
        switch notification.mType {
        case .friendRequest,
             .teamRequest,
             .tournamentRequest:
            
            buttonsView.isHidden = notification.mIsUsed ? true : false
        default:
            buttonsView.isHidden = true
        }
    }
    
    // MARK:- Private methods
    
    @objc private func acceptTap(_ sender: Any) {
        guard let notification = object else { return }
        delegate?.notificationViewCellAccept(self, notification: notification)
    }
    
    @objc private func declineTap(_ sender: Any) {
        guard let notification = object else { return }
        delegate?.notificationViewCellDecline(self, notification: notification)
    }
    
    private func setStatusText(type: UserNotification.NotificationType) {
        lblRequestType.aText = ("notificationView.lblType-" + type.rawValue).localized()
    }
    
    private func setButtonEnabled(_ enabled: Bool) {
        btnAccept.isEnabled = enabled
        btnDecline.isEnabled = enabled
    }
    
    private func setObject(with object: Any?, addition: String? = nil) {
        
        imgPreview.url = nil
        lblName.aText = ""
        lblSquadName.aText = ""
        
        switch object {
        case is User:
            
            guard let object = object as? User else { return }
            imgPreview.placeholder = UIImage(named: "common_imgContactPlaceholder-small")
            imgPreview.url = object.mPhoto?.mUrl
            lblName.aText = object.mFullName
            
        case is Team:
            
            guard let object = object as? Team else { return }
            imgPreview.placeholder = UIImage(named: "common_imgTeamPlaceholder-small")
            imgPreview.url = object.mPhoto?.mUrl
            lblName.aText = object.mName ?? ""
            
        case is Tournament:
            
            guard let object = object as? Tournament else { return }
            imgPreview.placeholder = UIImage(named: "common_imgTournamentPlaceholder-small")
            imgPreview.url = object.mPhoto?.mUrl
            lblName.aText = object.mName ?? ""
            
        default:
            break
        }
        
        if let addition = addition {
            lblSquadName.aText = addition.uppercased()
            lblSquadName.isHidden = false
        } else {
            lblSquadName.isHidden = true
        }
    }
}
